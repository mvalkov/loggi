package loggi

import (
	"fmt"
	"os"
	"time"
)

//Debug выводит отладочное сообщение
func Debug(v ...interface{}) {
	if os.Getenv("DEBUG") == "1" {
		fmt.Printf("[DEBUG] %s - ", timeStamp())
		fmt.Println(v...)
	}
}

//Info выводит информационное сообщение
func Info(v ...interface{}) {
	fmt.Printf("[INFO]  %s - ", timeStamp())
	fmt.Println(v...)
}

//Fatal выводит сообщение об ошибке
func Fatal(v ...interface{}) {
	fmt.Printf("[FATAL] %s - ", timeStamp())
	fmt.Println(v...)
	os.Exit(1)
}

func timeStamp() string {
	return time.Now().Format("2006-01-02 15:04:05")
}
